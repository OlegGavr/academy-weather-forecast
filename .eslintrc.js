module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: ['airbnb-typescript'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    project: './tsconfig.json'
  },
  plugins: [
    'react',
    '@typescript-eslint'
  ],
  settings: {
    react: {
      version: 'detect'
    }
  },
  rules: {
    'linebreak-style': 0,
    'import/prefer-default-export': 'off',
    'no-use-before-define': 'off',
    "react/react-in-jsx-scope": "off",
    '@typescript-eslint/no-use-before-define': ['error'],
    "no-console": "off",
    "func-names": [0],
    "max-len": [0],
    '@typescript-eslint/object-curly-spacing': [0],
    '@typescript-eslint/space-infix-ops': [0],
    'react/display-name': [0, { ignoreTranspilerName: true }],
    'react/destructuring-assignment': [0, {ignoreClassFields: true}],
    'react/jsx-props-no-spreading': [0, {html: "ingore"}],
    'react/require-default-props': [0]
  }
}
