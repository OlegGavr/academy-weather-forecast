/* eslint-disable no-unused-vars */

type Padding = `${number}px`;

export type PaddingType = {
  top: Padding;
  right: Padding;
  bottom: Padding;
  left: Padding;
}

export type PaddingTypeNull = {
  countType: 0;
}

export type PaddingTypeOne = {
  countType: 1;
  params: {
    padding: Padding;
  }
}

export type PaddingTypeTwo = {
  countType: 2;
  params: {
    paddingHorizontal: Padding;
    paddingVertical: Padding;
  }
}

export type PaddingTypeThree = {
  countType: 3;
  params: {
    paddingTop: Padding;
    paddingHorizontal: Padding;
    paddingBottom: Padding;
  }
}

export type PaddingTypeFour = {
  countType: 4;
  params: {
    paddingTop: Padding;
    paddingRight: Padding;
    paddingBottom: Padding;
    paddingLeft: Padding;
  }
}

export type PaddingTypeCount = PaddingTypeNull | PaddingTypeOne | PaddingTypeTwo | PaddingTypeThree | PaddingTypeFour;
