/* eslint-disable no-redeclare */

import {
  PaddingType, PaddingTypeCount, PaddingTypeFour,
  PaddingTypeNull, PaddingTypeOne, PaddingTypeThree, PaddingTypeTwo
} from './types'

const getPaddingNull = (): PaddingTypeNull => {
  return {
    countType: 0
  }
}

const getPaddingOne = (paddings: number[]): PaddingTypeOne => {
  const [padding] = paddings

  return {
    countType: 1,
    params: {
      padding: `${padding}px`
    }

  }
}

const getPaddingTwo = (paddings: number[]): PaddingTypeTwo => {
  const [paddingVertical, paddingHorizontal] = paddings

  return {
    countType: 2,
    params: {
      paddingVertical: `${paddingVertical}px`,
      paddingHorizontal: `${paddingHorizontal}px`
    }
  }
}

const getPaddingThree = (paddings: number[]): PaddingTypeThree => {
  const [paddingTop, paddingHorizontal, paddingBottom] = paddings

  return {
    countType: 3,
    params: {
      paddingTop: `${paddingTop}px`,
      paddingHorizontal: `${paddingHorizontal}px`,
      paddingBottom: `${paddingBottom}px`
    }
  }
}

const getPaddingFour = (paddings: number[]): PaddingTypeFour => {
  const [paddingTop, paddingRight, paddingBottom, paddingLeft] = paddings

  return {
    countType: 4,
    params: {
      paddingTop: `${paddingTop}px`,
      paddingRight: `${paddingRight}px`,
      paddingBottom: `${paddingBottom}px`,
      paddingLeft: `${paddingLeft}px`
    }
  }
}

function getStatePadding(): PaddingTypeNull;
function getStatePadding(padding: number): PaddingTypeOne;
function getStatePadding(paddingHorizontal: number, paddingVertical: number): PaddingTypeTwo;
function getStatePadding(paddingTop: number, paddingHorizontal: number, paddingBottom: number): PaddingTypeThree;
function getStatePadding(paddingTop: number, paddingRight: number, paddingBottom: number, paddingLeft: number): PaddingTypeFour;
function getStatePadding (...paddings: number[]): PaddingTypeCount {
  switch (paddings?.length) {
    case 1:
      return getPaddingOne(paddings)
    case 2:
      return getPaddingTwo(paddings)
    case 3:
      return getPaddingThree(paddings)
    case 4:
      return getPaddingFour(paddings)
    default:
      return getPaddingNull()
  }
}

function renderGetCountPadding (
  paddingTop?: number,
  paddingRight?: number,
  paddingBottom?: number,
  paddingLeft?: number
) {
  if (!paddingTop) {
    return getStatePadding()
  }

  if (!paddingRight) {
    return getStatePadding(paddingTop)
  }

  if (!paddingBottom) {
    return getStatePadding(paddingTop, paddingRight)
  }
  if (!paddingLeft) {
    return getStatePadding(paddingTop, paddingRight, paddingBottom)
  }

  return getStatePadding(paddingTop, paddingRight, paddingBottom, paddingLeft)
}

export function getPadding (
  paddingTop?: number,
  paddingRight?: number,
  paddingBottom?: number,
  paddingLeft?: number
): PaddingType {
  const statePadding: PaddingTypeCount = renderGetCountPadding(paddingTop, paddingRight, paddingBottom, paddingLeft)

  switch (statePadding.countType) {
    case 0:
      return {
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px'
      }
    case 1:
      return {
        top: statePadding.params.padding,
        right: statePadding.params.padding,
        bottom: statePadding.params.padding,
        left: statePadding.params.padding
      }
    case 2:
      return {
        top: statePadding.params.paddingVertical,
        right: statePadding.params.paddingHorizontal,
        bottom: statePadding.params.paddingVertical,
        left: statePadding.params.paddingHorizontal
      }
    case 3:
      return {
        top: statePadding.params.paddingTop,
        right: statePadding.params.paddingHorizontal,
        bottom: statePadding.params.paddingBottom,
        left: statePadding.params.paddingHorizontal
      }
    case 4:
      return {
        top: statePadding.params.paddingTop,
        right: statePadding.params.paddingRight,
        bottom: statePadding.params.paddingBottom,
        left: statePadding.params.paddingLeft
      }
  }
}
