import _ from 'lodash'

export function mergeObjects<T, V> (first: T, second: V): T & V {
  return _.cloneDeep({ ...first, ...second })
}
