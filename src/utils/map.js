/**
 * @template T, V
 * @param {(element: T) => V} callback
 * @param {T[]} collection
 * @return {V[]}
 */
export function mapWithReduce(callback, collection) {
  return collection.reduce((previous, element) => [...previous, callback(element)],
    []);
}
