/**
 * @template T
 * @param {(element: T) => boolean} predicate
 * @param {T[]} collection
 * @return {T[]}
 */
export function filterWithReduce(predicate, collection) {
  return collection.reduce((previous, element) => {
    if (predicate(element)) {
      return [...previous, element];
    }
    return previous;
  },
  []);
}
