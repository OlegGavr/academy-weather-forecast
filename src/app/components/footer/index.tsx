import React from 'react';
import { TitleMeta } from '../../common/text';
import styles from './styles.module.scss';

export function Footer() {
  return (
    <footer className={styles['footer-content']}>
      <TitleMeta className={styles.text}>
        C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT
      </TitleMeta>
    </footer>
  );
}
