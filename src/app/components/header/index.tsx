import React from 'react';
import { TitleH1 } from '../../common/text';
import styles from './styles.module.scss';

export function Header() {
  return (
    <header className={styles['header-content']}>
      <TitleH1 className={`${styles['header-title']} ${styles.first}`}>
        Weather
      </TitleH1>
      <TitleH1 className={`${styles['header-title']} ${styles.second}`}>
        forecast
      </TitleH1>
    </header>
  );
}
