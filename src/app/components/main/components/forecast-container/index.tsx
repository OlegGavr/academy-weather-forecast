import React from 'react';
import styles from './styles.module.scss';

type ForecastContainerProps = {
  header: React.ReactNode;
  content: React.ReactNode;
};

export function ForecastContainer(props: ForecastContainerProps) {
  return (
    <div>
      <div className={styles.header}>
        {props.header}
      </div>
      <div className={styles.content}>
        {props.content}
      </div>
    </div>
  );
}
