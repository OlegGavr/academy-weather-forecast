import React from 'react';
import { mockCities } from '../../../../../common/constants/city';
import { OptionType, Select } from '../../../../common/input/select';

type SelectForecastProps = {
  onChange(event: React.FormEvent<HTMLSelectElement>): void;
};

export function SelectForecast(props: SelectForecastProps) {
  const options: OptionType[] = mockCities.map((city) => ({
    key: city.value,
    text: city.text,
    value: city.value,
  }));

  return (
    <Select
      onChange={props.onChange}
      options={options}
    />
  );
}
