import React from 'react';
import { Rectangle } from '../../../../common/rectangle';
import { ForecastContainer } from '../forecast-container';
import { LeftSelect } from './components/select';
import { CityForecastContextProvider } from './context/provider';
import { LeftContent } from './components/content';

export function LeftForecast() {
  return (
    <CityForecastContextProvider>
      <Rectangle header="7 Days Forecast">
        <ForecastContainer
          header={<LeftSelect />}
          content={<LeftContent />}
        />
      </Rectangle>
    </CityForecastContextProvider>
  );
}
