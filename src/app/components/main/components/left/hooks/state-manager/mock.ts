import { CityForecastStateManager } from './types';

export const mockCityForecastStateManager: CityForecastStateManager = {
  state: {
    forecasts: [],
    loading: false,
  },
  methods: {
    setForecasts: () => console.log('noop'),
    setLoading: () => console.log('noop'),
  },
};
