import { useState } from 'react';
import { ForecastType } from '../../../../../../../typings/weather';
import { CityForecastStateManager } from './types';

export function useCityForecastContext(): CityForecastStateManager {
  const [forecasts, setForecasts] = useState<ForecastType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  return {
    state: {
      forecasts,
      loading,
    },
    methods: {
      setForecasts,
      setLoading,
    },
  };
}
