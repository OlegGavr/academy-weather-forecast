import { ForecastType } from '../../../../../../../typings/weather';

export type CityForecastState = {
  forecasts: ForecastType[];
  loading: boolean;
};

export type CityForecastMethods = {
  setForecasts(forecasts: ForecastType[]): void;
  setLoading(loading: boolean): void;
};

export type CityForecastStateManager = {
  state: CityForecastState;
  methods: CityForecastMethods
};
