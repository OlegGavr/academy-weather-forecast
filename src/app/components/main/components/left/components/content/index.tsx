import React, { useContext } from 'react';
import { Empty } from '../../../../../../common/empty';
import { Loader } from '../../../../../../common/loader';
import { CityForecastContext } from '../../context/context';
import { ListForecast } from '../list';

export function LeftContent() {
  const { stateManager } = useContext(CityForecastContext);

  const content = stateManager.state.forecasts.length
    ? (
      <ListForecast forecasts={stateManager.state.forecasts} />
    )
    : <Empty />;

  return stateManager.state.loading ? <Loader /> : content;
}
