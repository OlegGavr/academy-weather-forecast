import React from 'react';
import { SelectForecast } from '../../../select';
import { useCityForecast } from './hooks/city-forecast';

export function LeftSelect() {
  const { onChangeCity } = useCityForecast();

  return (
    <SelectForecast onChange={onChangeCity} />
  );
}
