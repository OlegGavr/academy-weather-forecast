import React, { useContext, useEffect, useState } from 'react';
import { mockCities } from '../../../../../../../../common/constants/city';
import { getCityForecast } from '../../../../../../../rest/city-forecast';
import { CityForecastContext } from '../../../context/context';

export function useCityForecast() {
  const { stateManager } = useContext(CityForecastContext);
  const [valueCity, setValueCity] = useState<string>('');

  const onChangeCity = (event: React.FormEvent<HTMLSelectElement>) => {
    setValueCity(event.currentTarget.value);
  };

  useEffect(() => {
    if (!valueCity) {
      return;
    }

    stateManager.methods.setLoading(true);

    const cityForecast = mockCities.find((city) => city.value === valueCity);
    getCityForecast(cityForecast?.x!, cityForecast?.y!)
      .then((forecastsInCity) => {
        const forecasts = forecastsInCity.daily.map((forecast: any) => ({ ...forecast, temp: forecast.temp.day }));
        stateManager.methods.setForecasts(forecasts);
        stateManager.methods.setLoading(false);
      });
  }, [valueCity]);

  return {
    onChangeCity,
  };
}
