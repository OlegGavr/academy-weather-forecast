import { useState } from 'react';
import { useMaxCount } from './count';

export function useCurrent() {
  const [current, setCurrent] = useState<number>(0);
  const max = useMaxCount();
  const isMin = current < 1;
  const isMax = current + max > 6;

  const onNext = () => {
    if (isMax) {
      return;
    }

    setCurrent(current + 1);
  };

  const onPrev = () => {
    if (isMin) {
      return;
    }

    setCurrent(current - 1);
  };

  return {
    current,
    max,
    isMin,
    isMax,
    onNext,
    onPrev,
  };
}
