import React from 'react';
import { ForecastType } from '../../../../../../../../typings/weather';
import { ForecastCard } from '../../../../../../../common/card';
import styles from '../styles.module.scss';

export function mapperListCards(forecasts: ForecastType[], current: number, max: number) {
  return forecasts.filter((_, index) => index >= current && index < current + max)
    .map((forecast) => (
      <ForecastCard
        className={styles['card-list']}
        key={forecast.dt}
        forecast={forecast}
        small
      />
    ));
}
