import { useEffect, useState } from 'react';

const MAX_COUNT = 3;
const MOBILE_MAX_COUNT = 5;

export function useMaxCount() {
  const [max, setMax] = useState<number>(MAX_COUNT);

  const handleResize = () => {
    if (window.innerWidth < 800) {
      setMax(MOBILE_MAX_COUNT);
    } else {
      setMax(MAX_COUNT);
    }
  };

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
  }, []);

  return max;
}
