import React from 'react';
import { ForecastType } from '../../../../../../../typings/weather';
import { LeftArrowSvg, RightArrowSvg } from '../../../../../../common/icons/arrows';
import { useCurrent } from './hooks/current';
import { mapperListCards } from './utils/cards';
import styles from './styles.module.scss';
import { HideButton } from '../../../../../../common/button';

type ListForecastProps = {
  forecasts: ForecastType[]
};

export function ListForecast(props: ListForecastProps) {
  const {
    current, max, isMin, isMax, onNext, onPrev,
  } = useCurrent();
  const cards = mapperListCards(props.forecasts, current, max);

  return (
    <div className={styles.content}>
      <HideButton
        className={styles.arrow}
        onClick={onPrev}
      >
        <LeftArrowSvg disabled={isMin} />
      </HideButton>
      <div className={styles.list}>
        {cards}
      </div>
      <HideButton
        className={styles.arrow}
        onClick={onNext}
      >
        <RightArrowSvg disabled={isMax} />
      </HideButton>
    </div>
  );
}
