import React from 'react';
import { WithChildren } from '../../../../../../common/types/props';
import { useCityForecastContext } from '../hooks/state-manager';
import { CityForecastContext } from './context';

type CityForecastContextProviderProps = WithChildren;

export function CityForecastContextProvider(props: CityForecastContextProviderProps) {
  const { children } = props;
  const stateManager = useCityForecastContext();

  return (
    <CityForecastContext.Provider value={{ stateManager }}>
      {children}
    </CityForecastContext.Provider>
  );
}
