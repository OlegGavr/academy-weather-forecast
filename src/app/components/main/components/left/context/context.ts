import React from 'react';
import { mockCityForecastStateManager } from '../hooks/state-manager/mock';
import { CityForecastStateManager } from '../hooks/state-manager/types';

export type CityForecastContextValue = {
  stateManager: CityForecastStateManager;
};

const initialValue: CityForecastContextValue = {
  stateManager: mockCityForecastStateManager,
};

export const CityForecastContext = React.createContext<CityForecastContextValue>(initialValue);
