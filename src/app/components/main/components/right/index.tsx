import React from 'react';
import { Rectangle } from '../../../../common/rectangle';
import { ForecastContainer } from '../forecast-container';
import { RightContent } from './components/content';
import { RightSelects } from './components/selects';
import { DateForecastContextProvider } from './context/provider';
import styles from './styles.module.scss';

export function RightForecast() {
  return (
    <DateForecastContextProvider>
      <Rectangle
        className={styles['right-content']}
        header="Forecast for a Date in the Past"
      >
        <ForecastContainer
          header={<RightSelects />}
          content={<RightContent />}
        />
      </Rectangle>
    </DateForecastContextProvider>
  );
}
