import moment from 'moment';

export function getDateRange() {
  const now = new Date();
  const oneDayMilliseconds = 24 * 60 * 60 * 1000;
  const fiveDaysMilliseconds = 5 * oneDayMilliseconds;
  const minDate = new Date(+now - fiveDaysMilliseconds);
  const maxDate = new Date(+now - oneDayMilliseconds);

  const maxDay = moment(maxDate, 'ss').format('YYYY-MM-DD');
  const minDay = moment(minDate, 'ss').format('YYYY-MM-DD');

  return {
    minDay,
    maxDay,
  };
}
