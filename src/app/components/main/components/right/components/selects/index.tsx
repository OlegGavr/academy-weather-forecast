import React from 'react';
import { DatePicker } from '../../../../../../common/input/date-picker';
import { SelectForecast } from '../../../select';
import { getDateRange } from './utils/date-range';
import { useDateForecast } from './hooks/date-forecast';
import styles from '../../styles.module.scss';

export function RightSelects() {
  const { onChangeSelect, onChangeDate } = useDateForecast();
  const { minDay, maxDay } = getDateRange();

  return (
    <div className={styles.header}>
      <SelectForecast onChange={onChangeSelect} />
      <DatePicker onChange={onChangeDate} max={maxDay} min={minDay} />
    </div>
  );
}
