import React, { useContext, useEffect, useState } from 'react';
import { mockCities } from '../../../../../../../../common/constants/city';
import { getDateForecast } from '../../../../../../../rest/date-forecast';
import { DateForecastContext } from '../../../context/context';

export function useDateForecast() {
  const { stateManager } = useContext(DateForecastContext);
  const [valueCity, setValueCity] = useState<string>('');
  const [date, setDate] = useState<number>(0);

  const onChangeSelect = (event: React.FormEvent<HTMLSelectElement>) => {
    setValueCity(event.currentTarget.value);
  };

  const onChangeDate = (event: React.FormEvent<HTMLInputElement>) => {
    const dateSeconds = +new Date(event.currentTarget.value) / 1000;
    setDate(dateSeconds);
  };

  useEffect(() => {
    if (!valueCity || !date) {
      return;
    }

    stateManager.methods.setLoading(true);

    const cityForecast = mockCities.find((city) => city.value === valueCity);
    getDateForecast(cityForecast?.x!, cityForecast?.y!, date)
      .then((res) => {
        stateManager.methods.setForecast(res.current);
        stateManager.methods.setLoading(false);
      });
  }, [valueCity, date]);

  return {
    onChangeDate,
    onChangeSelect,
  };
}
