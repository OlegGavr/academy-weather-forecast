import React, { useContext } from 'react';
import { ForecastCard } from '../../../../../../common/card';
import { Empty } from '../../../../../../common/empty';
import { Loader } from '../../../../../../common/loader';
import { DateForecastContext } from '../../context/context';

export function RightContent() {
  const { stateManager } = useContext(DateForecastContext);

  const content = stateManager.state.forecast
    ? (
      <ForecastCard forecast={stateManager.state.forecast} />
    )
    : <Empty />;

  return stateManager.state.loading ? <Loader /> : content;
}
