import React from 'react';
import { mockDateForecastStateManager } from '../hooks/state-manager/mock';
import { DateForecastStateManager } from '../hooks/state-manager/types';

export type DateForecastContextValue = {
  stateManager: DateForecastStateManager;
};

const initialValue: DateForecastContextValue = {
  stateManager: mockDateForecastStateManager,
};

export const DateForecastContext = React.createContext<DateForecastContextValue>(initialValue);
