import React from 'react';
import { WithChildren } from '../../../../../../common/types/props';
import { useDateForecastContext } from '../hooks/state-manager';
import { DateForecastContext } from './context';

type DateForecastContextProviderProps = WithChildren;

export function DateForecastContextProvider(props: DateForecastContextProviderProps) {
  const { children } = props;
  const stateManager = useDateForecastContext();

  return (
    <DateForecastContext.Provider value={{ stateManager }}>
      {children}
    </DateForecastContext.Provider>
  );
}
