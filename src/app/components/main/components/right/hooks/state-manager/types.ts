import { ForecastType } from '../../../../../../../typings/weather';

export type DateForecastState = {
  forecast?: ForecastType;
  loading: boolean;
};

export type DateForecastMethods = {
  setForecast(forecast: ForecastType): void;
  setLoading(loading: boolean): void;
};

export type DateForecastStateManager = {
  state: DateForecastState;
  methods: DateForecastMethods
};
