import { useState } from 'react';
import { ForecastType } from '../../../../../../../typings/weather';
import { DateForecastStateManager } from './types';

export function useDateForecastContext(): DateForecastStateManager {
  const [forecast, setForecast] = useState<ForecastType>();
  const [loading, setLoading] = useState<boolean>(false);

  return {
    state: {
      forecast,
      loading,
    },
    methods: {
      setForecast,
      setLoading,
    },
  };
}
