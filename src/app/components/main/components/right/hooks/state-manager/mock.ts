import { DateForecastStateManager } from './types';

export const mockDateForecastStateManager: DateForecastStateManager = {
  state: {
    forecast: {
      dt: 0,
      temp: 0,
      weather: [{
        description: '',
        icon: '',
        id: 0,
      }],
    },
    loading: false,
  },
  methods: {
    setForecast: () => console.log('noop'),
    setLoading: () => console.log('noop'),
  },
};
