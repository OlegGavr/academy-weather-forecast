import React from 'react';
import { LeftForecast } from './components/left';
import { RightForecast } from './components/right';
import styles from './styles.module.scss';

export function Main() {
  return (
    <main className={styles['main-content']}>
      <LeftForecast />
      <RightForecast />
    </main>
  );
}
