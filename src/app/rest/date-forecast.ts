import { API_ID } from './api';

export function getDateForecast(lat: number, lon: number, time: number): Promise<any> {
  return fetch(`https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lon}&dt=${time}&appid=${API_ID}`)
    .then((forecast) => forecast.json());
}
