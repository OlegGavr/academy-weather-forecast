import { API_ID } from './api';

export function getCityForecast(lat: number, lon: number): Promise<any> {
  return fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${API_ID}`)
    .then((forecast) => forecast.json());
}
