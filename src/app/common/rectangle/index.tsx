import React from 'react';
import { getClassName } from '../../../common/styles/classname';
import { DivProps } from '../../../common/types/props';
import { TitleH1 } from '../text';
import styles from './styles.module.scss';

type RectangleProps = DivProps & {
  header: string;
};

export function Rectangle(props: RectangleProps) {
  const className = getClassName(styles.rectangle, props.className);

  return (
    <div {...props} className={className}>
      <TitleH1 className={styles.header}>
        {props.header}
      </TitleH1>
      <div className={styles.content}>
        {props.children}
      </div>
    </div>
  );
}
