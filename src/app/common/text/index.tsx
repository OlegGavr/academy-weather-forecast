import _TitleMeta from './components/meta';
import _TitleH2 from './components/h2';
import _TitleH1 from './components/h1';
import _TitleBody from './components/body';
import _TitleBodyBold from './components/body-bold';

export const TitleMeta = _TitleMeta;
export const TitleH2 = _TitleH2;
export const TitleH1 = _TitleH1;
export const TitleBody = _TitleBody;
export const TitleBodyBold = _TitleBodyBold;
