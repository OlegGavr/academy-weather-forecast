import React from 'react';
import { ParagraphProps } from '../../../../common/types/props';
import TitleMeta from './meta';
import styles from '../styles.module.scss';
import { getClassName } from '../../../../common/styles/classname';

export default function (props: ParagraphProps) {
  const className = getClassName(styles.h2, props.className);

  return (
    <TitleMeta {...props} className={className} />
  );
}
