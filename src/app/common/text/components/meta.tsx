import React from 'react';
import { getClassName } from '../../../../common/styles/classname';
import { ParagraphProps } from '../../../../common/types/props';
import styles from '../styles.module.scss';

export default function (props: ParagraphProps) {
  const className = getClassName(styles.title, props.className);

  return (
    <p {...props} className={className}>
      {props.children}
    </p>
  );
}
