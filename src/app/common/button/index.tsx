import React from 'react';
import { getClassName } from '../../../common/styles/classname';
import { ButtonProps } from '../../../common/types/props';
import styles from './styles.module.scss';

export function HideButton(props: ButtonProps) {
  const className = getClassName(styles['hide-button'], props.className);

  return (
    <button {...props} type="button" className={className}>
      {props.children}
    </button>
  );
}
