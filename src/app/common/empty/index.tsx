import React from 'react';
import { PlaceholderSvg } from '../icons/placeholder';
import { TitleBodyBold } from '../text';
import styles from './styles.module.scss';

export function Empty() {
  return (
    <div className={styles.empty}>
      <PlaceholderSvg />
      <TitleBodyBold className={styles.message}>
        Fill in all the fields and the weather will be displayed
      </TitleBodyBold>
    </div>
  );
}
