import React from 'react';
import { getClassName } from '../../../../common/styles/classname';
import { InputProps } from '../../../../common/types/props';
import styles from '../styles.module.scss';

export function DatePicker(props: InputProps) {
  const className = getClassName(styles['date-picker'], props.className);

  return (
    <input
      {...props}
      className={className}
      type="date"
      placeholder="Select date"
    />
  );
}
