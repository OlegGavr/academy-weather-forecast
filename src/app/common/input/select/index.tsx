import React from 'react';
import { getClassName } from '../../../../common/styles/classname';
import { SelectProps } from '../../../../common/types/props';
import styles from '../styles.module.scss';

export type OptionType = {
  key: string;
  value: string;
  text: string;
};

type SelectComponentProps = SelectProps & {
  options: OptionType[]
};

export function Select(props: SelectComponentProps) {
  const className = getClassName(styles.select, props.className);

  const mapOptions = props.options.map((option) => (
    <option key={option.key} value={option.value}>
      {option.text}
    </option>
  ));

  const emptyOption = (
    <option
      key="none"
      hidden
      value={undefined}
    >
      Select city
    </option>
  );

  const options = [
    emptyOption,
    ...mapOptions,
  ];

  return (
    <select {...props} className={className}>
      {options}
    </select>
  );
}
