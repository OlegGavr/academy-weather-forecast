import moment from 'moment';

export function getDate(date: number) {
  const originDate = new Date(date * 1000);

  return moment(originDate, 'ss')
    .format('D MMM YYYY')
    .toLowerCase();
}
