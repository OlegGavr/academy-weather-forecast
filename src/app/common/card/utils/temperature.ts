export function getTemperature(temp: number) {
  const celsius = Math.round(temp - 273.15);
  return celsius > 0 ? `+${celsius}` : celsius;
}
