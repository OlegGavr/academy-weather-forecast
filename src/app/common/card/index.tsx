import React from 'react';
import { ForecastType } from '../../../typings/weather';
import { TitleBodyBold, TitleH1 } from '../text';
import { getImageUrl } from './utils/image-url';
import styles from './styles.module.scss';
import { getTemperature } from './utils/temperature';
import { getDate } from './utils/date';
import { getClassName } from '../../../common/styles/classname';

type ForecastCardProps = {
  forecast: ForecastType;
  className?: string;
  small?: boolean;
};

export function ForecastCard(props: ForecastCardProps) {
  const { forecast } = props;
  const date = getDate(forecast.dt);
  const temperature = getTemperature(forecast.temp);
  const className = getClassName(styles.card, props.className);
  const small = props.small ? styles.small : '';
  const imgClassName = getClassName(styles.img, small);

  return (
    <div className={className}>
      <TitleBodyBold className={styles.text}>
        {date}
      </TitleBodyBold>
      <span className={styles.icon}>
        <img className={imgClassName} src={getImageUrl(forecast.weather[0].icon)} alt="icon" />
      </span>
      <TitleH1 className={`${styles.text} ${styles.temp}`}>
        {temperature}
        &deg;
      </TitleH1>
    </div>
  );
}
