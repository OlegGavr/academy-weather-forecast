import React from 'react';
import { getClassName } from '../../../../common/styles/classname';
import { SvgOptionProps } from '../../../../common/types/props';
import styles from '../styles.module.scss';
import arrowStyles from './styles.module.scss';

export function LeftArrowSvg(props: SvgOptionProps) {
  const base = `${styles.icon} ${props.disabled ? styles.disabled : ''}`;
  const className = getClassName(base, props.className);

  return (
    <svg {...props} className={className} width="2.4rem" height="2.4rem" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M9.41424 12.0001L16.7071 19.293L15.2929 20.7072L6.58582 12.0001L15.2929 3.29297L16.7071 4.70718L9.41424 12.0001Z" fill="#8083A4" />
    </svg>
  );
}

export function RightArrowSvg(props: SvgOptionProps) {
  const className = getClassName(arrowStyles['arrow-tranform'], props.className);

  return (
    <LeftArrowSvg {...props} className={className} />
  );
}

export function TopArrowSvg(props: SvgOptionProps) {
  const base = `${styles.icon} ${props.disabled ? styles.disabled : ''}`;
  const className = getClassName(base, props.className);

  return (
    <svg {...props} className={className} width="1.6rem" height="1.6rem" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M8.00001 6.41436L2.70712 11.7073L1.29291 10.293L8.00001 3.58594L14.7071 10.293L13.2929 11.7073L8.00001 6.41436Z" fill="#8083A4" />
    </svg>
  );
}

export function BottomArrowSvg(props: SvgOptionProps) {
  const className = getClassName(arrowStyles['arrow-tranform'], props.className);

  return (
    <TopArrowSvg {...props} className={className} />
  );
}
