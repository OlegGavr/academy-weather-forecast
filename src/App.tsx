import React from 'react';
import { Footer } from './app/components/footer';
import { Header } from './app/components/header';
import { Main } from './app/components/main';

export function App() {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
}
