export function getClassName(base: string, option?: string) {
  return option ? `${base} ${option}` : base;
}
