type CitiesType = {
  value: string;
  text: string;
  x: number;
  y: number;
};

export const mockCities: CitiesType[] = [
  {
    value: 'SAMARA',
    text: 'Самара',
    x: 53.195873,
    y: 50.100193,
  },
  {
    value: 'TOGLIATTI',
    text: 'Тольятти',
    x: 53.507836,
    y: 49.420393,
  },
  {
    value: 'SARATOV',
    text: 'Саратов',
    x: 51.533557,
    y: 46.034257,
  },
  {
    value: 'KAZAN',
    text: 'Казань',
    x: 55.796127,
    y: 49.106405,
  },
  {
    value: 'KRASNODAR',
    text: 'Краснодар',
    x: 45.035470,
    y: 38.975313,
  },
];
