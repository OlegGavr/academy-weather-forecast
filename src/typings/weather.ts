export type WeatherType = {
  description: string;
  icon: string;
  id: number;
};

export type ForecastType = {
  dt: number;
  temp: number;
  weather: WeatherType[];
};
